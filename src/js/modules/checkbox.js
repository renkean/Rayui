/*
 @Name: checkbox插件
 @Author: ray
 @License: MIT
*/
; rayui.define(function (exports, undef) {
    "use strict";

    var $ = rayui.$,
        plugName = "checkbox",
        checkedClass = "chkbox-checked",
        disabledClass = "rayui-disabled",
        selector = '.rayui-chkbox-box:not(".' + disabledClass + '")',
        render = function (container) {
            //初始化checkbox
            var $objs = container ? $(".rayui-checkbox", container) : $(".rayui-checkbox");
            $objs.each(function () {
                var that = this, $elem = $(that);
                if ($elem.data("render")) $elem.next().remove();
                $elem.data("render", true);
                var raysplit = $elem.attr("ray-split") || '|',
                    value = function () {
                        var val = $elem.attr("ray-value") || "";
                        if (val === "") return [true, false];

                        var list = val.split(raysplit);
                        list.length === 1 && list.push("");

                        if (list[0] === "true" && (list[1] === "" || list[1] === "false")) return [true, false];
                        if (list[0] === "1" && (list[1] === "" || list[1] === "0")) return [1, 0];

                        if (list[1] === "") {
                            if (list[0] === "on") return ["on", "off"];
                            list[1] = "&nbsp;";
                        }
                        return list;
                    }(),
                    disabled = $elem.attr("disabled") || $elem.attr("readonly"),
                    skin = $elem.attr("skin"),
                    checked = that.checked,
                    title = $elem.attr("ray-title") || "";

                //创建dom
                var $main = $(['<div class="rayui-chkbox-box',
                    checked ? " " + checkedClass : "",
                    disabled ? " " + disabledClass : "",
                    '"',//end class
                    ' skin=' + (skin || "default") + '>',
                    '<i class="ra ra-right chkbox-icon"></i>',
                    '</div>'
                ].join('')).insertAfter($elem);
                if (title !== "") {
                    $('<span class="chkbox-title">' + title + '</span>').appendTo($main);
                }
                $main.data("value", value);
                //绑定事件
                $main.on("click." + plugName, function () {
                    if ($(this).hasClass(disabledClass)) return;
                    var toChecked = $(this).hasClass(checkedClass) ? 1 : 0;
                    $(this).toggleClass(checkedClass);
                    that.checked = toChecked === 0;
                });
            });
        };

    var chkbox = {
        on: function (event, callback, container) {
            if (event !== "check" || typeof callback !== "function") return this;
            container = container || "body";
            $(container).on("click." + plugName, selector, function (e) {
                var $this = $(this),
                    $elem = $this.prev(),
                    value = $this.data("value"),
                    rayevent = $elem.attr("ray-event"),
                    curr = $(this).hasClass(checkedClass) ? 0 : 1;

                rayevent && callback.call(this, {
                    event: rayevent,
                    elem: $elem,
                    checked: value[curr],
                    e: e
                });
            });
            return this;
        },
        off: function (event, container) {
            if (event !== "check") return this;
            container = container || "body";
            $(container).off("click." + plugName, selector);
            return this;
        },
        enable: function (elems) {
            $(elems).each(function () {
                $(this).prop("disabled", false).next().removeClass(disabledClass);
            });
            return this;
        },
        disable: function (elems) {
            $(elems).each(function () {
                $(this).prop("disabled", true).next().addClass(disabledClass);
            });
            return this;
        },
        setReadonly: function (elems, readflag) {
            $(elems).each(function () {
                $(this).prop("readonly", readflag).next()[readflag ? "addClass" : "removeClass"](disabledClass);
            });
            return this;
        },
        check: function (elems) {
            return this.toggle(elems, true);
        },
        unCheck: function (elems) {
            return this.toggle(elems, false);
        },
        toggle: function (elems, toCheck) {
            $(elems).not("[disabled]").not("[readonly]").each(function () {
                var $main = $(this).next(),
                    isChecked = $main.hasClass(checkedClass),
                    tmpToChk = toCheck === undef ? !isChecked : toCheck;
                if (isChecked === tmpToChk) return;
                this.checked = tmpToChk;
                $main.toggleClass(checkedClass);
            });
            return this;
        },
        click: function (elems) {
            $(elems).each(function () {
                $(this).next().click();
            });
            return this;
        },
        render: function (container) {
            render(container);
        }
    };

    exports(plugName, chkbox);
}, rayui.jsAsync());
