/*
 @Name: lazy插件
 @Author: ray
 @License: MIT
*/
; rayui.define(function (exports) {
    "use strict";

    var $ = rayui.$,
        $win = $(window),
        $doc = $(document),
        elemOpts = [],
        isDocBind = false,
        plugName = "lazy",
        globalOptions = {
            imgClass: null,
            gap: 50,
            srcAttr: "lazy-src"
        },
        render = function (options) {
            var winW = $win.width(), winH = $win.height(),
                dscrollLeft = $doc.scrollLeft(), dscrollTop = $doc.scrollTop(),
                minW = winW, minH = winH;
            if (!options.isdoc) {
                var eOffset = $(options.elem).offset();
                minW = Math.min(winW, $(options.elem).width() + eOffset.left - dscrollLeft);
                minH = Math.min(winH, $(options.elem).height() + eOffset.top - dscrollTop);
            }
            minW += options.gap;
            minH += options.gap;
            var $objs = $(options.elem).find((options.imgClass || "img") + "[" + options.srcAttr + "]:visible");
            $objs.each(function () {
                var $img = $(this);
                var offset = $img.offset();
                var dLeft = $doc.scrollLeft();
                var dTop = $doc.scrollTop();
                if (offset.top - dTop < minH && offset.left - dLeft < minW) {
                    $img.prop("src", $img.attr(options.srcAttr)).removeAttr(options.srcAttr);
                }
            });
        },
        callRender = function (options) {
            if (options.time) clearTimeout(options.time);
            options.time = setTimeout(function () {
                render(options);
                options.time = null;
            }, 50);
        };


    var DealClass = function (options) {
        this.options = options;

        elemOpts.push(options);
        options.isdoc = $(options.elem).is($doc);
        //区分是否是document，否则一页上有多个区域懒加载，document会绑定多次
        if (!options.isdoc) {
            if (!isDocBind) {
                isDocBind = true;
                $doc.on("scroll." + plugName, function () {
                    $.each(elemOpts, function (a, b) {
                        !b.isdoc && callRender(b);
                    });
                });
            }
        }
        render(options);
        $(options.elem).on("scroll." + plugName, function () {
            callRender(options);
        });

        return this;
    };

    DealClass.prototype = {
        //手动触发渲染
        rerender: function () {
            callRender(this.options);
            return this;
        }
    };

    var timer;
    $win.resize(function () {
        if (timer != null) clearTimeout(timer);
        timer = setTimeout(function () {
            $.each(elemOpts, function (a, b) {
                callRender(b);
            });
            timer = null;
        }, 20);
    });



    var handle = {
        options: globalOptions,
        render: function (elem, options) {
            options = options || {};
            options = $.extend(true, { elem: elem }, DealClass.option, options, $.isPlainObject(elem) && elem);
            return new DealClass(options);
        }
    }

    exports(plugName, handle);
}, rayui.jsAsync());
