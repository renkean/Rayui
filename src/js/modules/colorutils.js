/*
 @Name: colorutils插件
 @Author: ray
 @License: MIT
 说明：方法复制的jQuery colorpicker方法
           hsb{h：0-360, s:0-100, b:0-100}
           rgb都是0-255
*/
; rayui.define(function (exports) {
    "use strict";

    var $ = rayui.$,
        plugName = "colorutils";
    
    var colorpg = {
        //颜色相互转换
        rgb2hsb: function (rgb) {
            var hsb = { h: 0, s: 0, b: 0 };
            var min = Math.min(rgb.r, rgb.g, rgb.b);
            var max = Math.max(rgb.r, rgb.g, rgb.b);
            var delta = max - min;

            hsb.b = max;
            hsb.s = max === 0 ? 0 : 255 * delta / max;            
            if (delta !== 0) {
                if (rgb.r === max) {
                    hsb.h = (rgb.g - rgb.b) / delta;
                    if (rgb.g < rgb.b) hsb.h += 6;
                } else if (rgb.g === max) {
                    hsb.h = 2 + (rgb.b - rgb.r) / delta;
                } else {
                    hsb.h = 4 + (rgb.r - rgb.g) / delta;
                }
            }
            hsb.h = Math.round(hsb.h * 60);
            hsb.s *= 100 / 255;
            hsb.b *= 100 / 255;

            return hsb;
        },
        rgb2hex: function (rgb) {
            var hex = [
                rgb.r.toString(16),
                rgb.g.toString(16),
                rgb.b.toString(16)
            ];
            $.each(hex, function (nr, val) {
                if (val.length === 1) {
                    hex[nr] = '0' + val;
                }
            });
            return hex.join('');
        },
        hsb2rgb: function (hsb) {
            var rgb = {};
            var h = Math.round(hsb.h);
            var s = Math.round(hsb.s * 255 / 100);
            var v = Math.round(hsb.b * 255 / 100);
            if (s == 0) {
                rgb.r = rgb.g = rgb.b = v;
            } else {
                var t1 = v;
                var t2 = (255 - s) * v / 255;
                var t3 = (t1 - t2) * (h % 60) / 60;
                if (h == 360) h = 0;
                if (h < 60) { rgb.r = t1; rgb.b = t2; rgb.g = t2 + t3 }
                else if (h < 120) { rgb.g = t1; rgb.b = t2; rgb.r = t1 - t3 }
                else if (h < 180) { rgb.g = t1; rgb.r = t2; rgb.b = t2 + t3 }
                else if (h < 240) { rgb.b = t1; rgb.r = t2; rgb.g = t1 - t3 }
                else if (h < 300) { rgb.b = t1; rgb.g = t2; rgb.r = t2 + t3 }
                else if (h < 360) { rgb.r = t1; rgb.g = t2; rgb.b = t1 - t3 }
                else { rgb.r = 0; rgb.g = 0; rgb.b = 0 }
            }
            return { r: Math.round(rgb.r), g: Math.round(rgb.g), b: Math.round(rgb.b) };
        },
        hsb2hex: function (hsb) {
            return this.rgb2hex(this.hsb2rgb(hsb));
        },
        hex2rgb: function (hex) {
            //#123转#112233格式
            if (hex.length === 4) {
                var tmp = "#";
                for (var i=1; i<4;i++) {
                    var c = hex.charAt(i);
                    tmp += c + c;
                }
                hex = tmp;
            }
            hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
            return { r: hex >> 16, g: (hex & 0x00FF00) >> 8, b: (hex & 0x0000FF) };
        },
        hex2hsb: function (hex) {
            return this.rgb2hsb(this.hex2rgb(hex));
        }
    };

    exports(plugName, colorpg);
}, rayui.jsAsync());
