/*
 @Name: timerbtn插件
 @Author: ray
 @License: MIT
 */
; rayui.define(function (exports, undef) {
    "use strict";

    var $ = rayui.$,
        plugName = "timerbtn",
        globalIndex = 1000,
        dataCache = {},
        timerBtnOption = {
            min: 0,
            max: 10,
            auto: true,
            down: true,
            step: 1,
            interval: 1000,
            unit: 's',
            disabledClass: true
        };

    /**
     * 倒计时按钮，一个按钮时返回handle，多个按钮时原样返回
     * @param {any} options
     */
    $.fn.timerBtn = function (options) {
        options = options || {};
        var funcInit = function () {
            var $this = $(this).addClass("rayui-timerbtn"), index = $this.data("index");
            if (!index) {
                index = globalIndex++;
                $this.data("index", index);
                var curOpt = $.extend(true, {}, timerBtnOption, options),
                    hasFormat = typeof curOpt.format === "function",
                    isinput = this.tagName === "INPUT";
                $.extend(true, curOpt, { timerIndex: -1, rate: 1});
                curOpt.align && $this.addClass('rayui-timerbtn-' + curOpt.align);
                var dotIndex = ("" + curOpt.step).indexOf(".");
                if (dotIndex >= 0) {
                    curOpt.decimal = ("" + curOpt.step).length - dotIndex - 1;
                    //计算放大比率
                    curOpt.rate = Math.pow(10, curOpt.decimal);
                    curOpt.step *= curOpt.rate;
                    curOpt.max *= curOpt.rate;
                    curOpt.min *= curOpt.rate;
                }
                var func = function (first) {
                    if (curOpt.stop ||
                        curOpt.down && curOpt.cur <= curOpt.min ||
                        !curOpt.down && curOpt.cur >= curOpt.max) {
                        clearInterval(curOpt.timerIndex);
                        curOpt.timerIndex = -1;
                        $this.removeClass('rayui-disabled')[isinput ? "val" : "html"](curOpt.defTitle);
                        return;
                    }
                    !first && (curOpt.down ? curOpt.cur -= curOpt.step : curOpt.cur += curOpt.step);
                    var text = curOpt.decimal === 0 ? curOpt.timerText + "(" + curOpt.cur + curOpt.unit + ")" : curOpt.timerText + "(" + (curOpt.cur / curOpt.rate).toFixed(curOpt.decimal) + curOpt.unit + ")";
                    hasFormat && (text = curOpt.format(text) || text);
                    $this[isinput ? "val" : "html"](text);
                }, obj = dataCache[index] =
                    {
                        start: function () {
                            if (curOpt.timerIndex < 0) {
                                curOpt.cur = curOpt.down ? curOpt.max : curOpt.min;
                                curOpt.stop = false;
                                curOpt.defTitle = $this[isinput ? "val" : "html"]();
                                curOpt.timerText === undef && (curOpt.timerText = curOpt.defTitle);
                                curOpt.disabledClass && $this.addClass('rayui-disabled');
                                curOpt.timerIndex = setInterval(func, curOpt.interval);
                                func(true);
                            }
                        },
                        stop: function () {
                            curOpt.stop = true;
                        }
                    };

                $this.click(function (e) {
                    if (curOpt.layerIndex > -1) {
                        e.stopImmediatePropagation();
                        return false;
                    }
                    curOpt.auto && obj.start();
                });
                return obj;
            }
            return dataCache[index];
        };

        //一个设备，有返回
        if ($(this).length === 1)
            return funcInit.call(this);

        //支持多个，无返回。可通过$("oneSelector").loadingBtn().start()进行控制
        $(this).each(function () {
            funcInit.call(this);
        });
        return $(this);

    }
    
    var handle = {
        timerBtnOption: timerBtnOption,
        render: function (options) {
            if ($(options.elem).length === 0) return "DOM对象不存在";
            return $(options.elem).timerBtn(options);
        }
    }
    
    exports(plugName, handle);
}, rayui.jsAsync());
