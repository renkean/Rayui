/*
 @Name: loadingbtn插件
 @Author: ray
 @License: MIT
 */
; rayui.define("layer", function (exports, undef) {
    "use strict";

    var $ = rayui.$,
        layer = rayui.layer,
        plugName = "loadingbtn",
        globalIndex = 1000,
        dataCache = {},
        loadingBtnOption = {
            type: 0,
            icon: 0,
            auto: true,
            right: false,
            disabledClass: true
        };

    //获取等待按钮的旋转属性
    function getlineOptions($this, options) {
        if (options.icon !== 0) return {};

        var obj = {
            rotate: 2, //距离圆点位置
            lines: 10, //线条数量
            length: 5, //线条长度
            lheight: 2, //线条宽度
            color: $this.css('color')
        }

        if ($this.hasClass("btn-3x") || $this.hasClass("btn-4x")) {
            obj.length = 6;
        }
        else if ($this.hasClass("btn-3x-sh") || $this.hasClass("btn-4x-sh")) {
            obj.length = 4;
            obj.lheight = 1.4;
        }
        return obj;
    }

    /**
     * 等待按钮，一个按钮时返回handle，多个按钮时原样返回
     * @param {any} options
     */
    $.fn.loadingBtn = function (options) {
        options = options || {};
        options.time = 0;//取消time参数
        var func = function () {
            var $this = $(this), index = $this.data("index");
            if (!index) {
                index = globalIndex++;
                $this.data("index", index);
                $this.addClass("rayui-loadingbtn");
                var curOpt = $.extend(true, {}, loadingBtnOption, options);
                $.extend(true, curOpt, {
                    container: $this,
                    shade: false,
                    loadOpt0: getlineOptions($this, curOpt),
                    autoResize: false
                });
                if (curOpt.right) {
                    curOpt.offset = 6;
                    $this.addClass('rayui-loadingbtn-right');
                } else {
                    curOpt.onShow = function () {
                        $(this).css({//此时的this不是$this
                            top: 3,
                            left: 0,
                            "margin-left": "5px"
                        });
                    }
                }
                var stopFunc = function () {
                    $this.html(curOpt.defTitle).removeClass('rayui-disabled');
                    curOpt.layerIndex = -1;
                    curOpt.waitIndex && clearTimeout(curOpt.waitIndex);
                    curOpt.waitIndex = null;
                },
                    obj = dataCache[index] =
                        {
                            start: function (waittime) {
                                if (curOpt.layerIndex > -1) return;
                                curOpt.defTitle = $this.html();
                                curOpt.loadingText === undef && (curOpt.loadingText = curOpt.defTitle);
                                $this.html(curOpt.loadingText);
                                curOpt.layerIndex = layer.loading(curOpt);
                                curOpt.disabledClass && $this.addClass('rayui-disabled');
                                waittime = waittime || 30000;
                                curOpt.waitIndex && clearTimeout(curOpt.waitIndex);
                                curOpt.waitIndex = setTimeout(this.stop, waittime);
                            },
                            stop: function () {
                                layer.close(curOpt.layerIndex);
                                stopFunc();
                            }
                        };
                //如果有关闭事件
                if (curOpt.onClose) {
                    curOpt.onClose2 = curOpt.onClose;
                    curOpt.onClose = function () {
                        stopFunc();
                        curOpt.onClose2.call($this);
                    }
                }
                $this.click(function (e) {
                    if (curOpt.layerIndex > -1) {
                        e.stopImmediatePropagation();
                        return false;
                    }
                    curOpt.auto && obj.start();
                });
                return obj;
            }
            return dataCache[index];
        }

        //一个设备，有返回
        if ($(this).length === 1)
            return func.call(this);

        //支持多个，无返回。可通过$("oneSelector").loadingBtn().start()进行控制
        $(this).each(function () {
            func.call(this);
        });
        return $(this);
    }

    var handle = {
        loadingBtnOption: loadingBtnOption,
        render: function (options) {
            if ($(options.elem).length === 0) return "DOM对象不存在";
            return $(options.elem).loadingBtn(options);
        }
    }
    
    exports(plugName, handle);
}, rayui.jsAsync());
