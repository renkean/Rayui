/*
 @Name: jqlibs公共插件
 @Author: ray
 @License: MIT
*/
; rayui.define(function (exports, undef) {
    "use strict";

    $.isJqueryDom = function (dom) {
        return dom instanceof jQuery;
    }

    $.htmlEncode = function (value) {
        if (value) {
            return $('<div />').text(value).html();
        } else {
            return '';
        }
    }

    $.htmlDecode = function (value) {
        if (value) {
            return $('<div />').html(value).text();
        } else {
            return '';
        }
    }

    $.html2Escape = function (sHtml) {
        return sHtml.replace(/[<>&"]/g, function (c) { return { '<': '&lt;', '>': '&gt;', '&': '&amp;', '"': '&quot;' }[c]; });
    }

    $.escape2Html = function (str) {
        var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
        return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) { return arrEntities[t]; });
    }

    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return "";
    }

    //onScriptError当jsonp异常失败时调用
    $.rayJsonp = function (option) {
        var xhr = $.ajax(option);
        // ie 8+, chrome and some other browsers
        var head = document.head || $('head')[0] || document.documentElement; // code from jquery
        var script = $(head).find('script')[0];
        script.onerror = function (evt) {
            if (typeof option.onScriptError === "function")
                option.onScriptError(evt);
            //delete script elem
            if (script.parentNode) script.parentNode.removeChild(script);
            // delete jsonCallback global function
            if (!!option.jsonpCallback) return;
            var src = script.src || '', callback = option.jsonp || "callback";
            var idx = src.indexOf(callback + '=');
            if (idx != -1) {
                var idx2 = src.indexOf('&', idx);
                if (idx2 == -1) {
                    idx2 = src.length;
                }
                var jsonCallback = src.substring(idx + 13, idx2);
                delete window[jsonCallback];
            }
        };
        return xhr;
    }

    $.stopBack = function () {
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, document.URL);
        });
    }

    Array.prototype.search = function (key, val, children, callback) {
        if (this.hasOwnProperty(key)) return this;
        if (typeof children === "function") {
            callback = children;
            children = "children";
        } else {
            children = children || "children";
        }
        var data = this, newData = [], count = data.length, i = 0, a, index;
        for (; i < count; i++) {
            a = data[i];
            index = a[key].indexOf(val);
            if (index >= 0) {
                callback && callback.call(this, a);
                newData.push(a);
                continue;
            }
            if (a.hasOwnProperty(children)) {
                var list = a[children].search(key, val, children, callback);
                if (list.length > 0) {
                    var obj = {};
                    obj[children] = list;
                    var dd = $.extend({}, a, obj);
                    newData.push(dd);
                }
            }
        }
        return newData;
    }


    exports("jqlibs", {});
}, rayui.jsAsync())