/*
 @Name: switch插件
 @Author: ray
 @License: MIT
*/
; rayui.define(function (exports, undef) {
    "use strict";

    var $ = rayui.$,
        plugName = "switchbtn",
        offClass = "switch-off",
        gbIndex=1000,
        disabledClass = "rayui-disabled",
        selector = '.rayui-switch-box:not(".' + disabledClass + '")',
        render = function (container) {
            //初始化checkbox
            var $objs = container ? $(".rayui-switch", container) : $(".rayui-switch");
            $objs.each(function () {
                var that = this, $elem = $(that);
                if ($elem.data("render")) $elem.next().remove();
                $elem.data("render", true);
                var format = function (val) {
                    //val.length肯定>=1
                    val.length === 1 && val.push("");
                    if (val[1] === "") {
                        if (val[0] === "开") return ["开", "关"];
                        if (val[0] === "on") return ["on", "off"];
                        val[1] = "&nbsp;";
                    }
                    return val;
                },
                    domIndex = gbIndex++,
                    raysize = $elem.attr("ray-size"),
                    raysplit = $elem.attr("ray-split") || '|',
                    value = format(($elem.attr("ray-value") || "on|off").split(raysplit)),
                    raybgcolors = ($elem.attr("ray-bg-color") || "").split(raysplit),
                    raytextcolors = ($elem.attr("ray-text-color") || "").split(raysplit),
                    rayhitcolors = ($elem.attr("ray-hit-color") || "").split(raysplit),
                    disabled = $elem.attr("disabled") || $elem.attr("readonly"),
                    isOn = $elem.val() === value[0],
                    borderW = 2,
                    title = format(($elem.attr("ray-title") || "&nbsp;|&nbsp;").split(raysplit));
                if (title.length < 2) title.push("&nbsp;");

                //背景色
                var cssStr = ['<style>'];
                if (raybgcolors.length === 2) {
                    if (raybgcolors[0] !== "") {
                        cssStr.push('.rayui-switch-' + domIndex + '{');
                        cssStr.push('border-color:' + raybgcolors[0] + ';');
                        cssStr.push('background-color:' + raybgcolors[0] + ';');
                        cssStr.push('}');
                    }
                    if (raybgcolors[1] !== "") {
                        cssStr.push('.rayui-switch-' + domIndex + '[class*=' + offClass +']{');
                        cssStr.push('border-color:' + raybgcolors[1] + ';');
                        cssStr.push('background-color:' + raybgcolors[1] + ';');
                        cssStr.push('}');
                    }
                }

                if (raytextcolors.length === 2) {
                    if (raytextcolors[0] !== "") {
                        cssStr.push('.rayui-switch-' + domIndex + ' .switch-0{');
                        cssStr.push('color:' + raytextcolors[0] + ';');
                        cssStr.push('}');
                    }
                    if (raytextcolors[1] !== "") {
                        cssStr.push('.rayui-switch-' + domIndex + ' .switch-1{');
                        cssStr.push('color:' + raytextcolors[1] + ';');
                        cssStr.push('}');
                    }
                }

                if (rayhitcolors.length === 2) {
                    if (rayhitcolors[0] !== "") {
                        cssStr.push('.rayui-switch-' + domIndex + ' .switch-hit{');
                        cssStr.push('background-color:' + rayhitcolors[0] + ';');
                        cssStr.push('}');
                    }
                    if (rayhitcolors[1] !== "") {
                        cssStr.push('.rayui-switch-' + domIndex + '[class*=' + offClass + '] .switch-hit{');
                        cssStr.push('background-color:' + rayhitcolors[1] + ';');
                        cssStr.push('}');
                    }
                }

                cssStr.push('</style>');
                cssStr.length > 15 && $('body').append(cssStr.join(''));

                //创建dom
                var $main = $(['<div class="rayui-switch-box rayui-switch-' + domIndex,
                    isOn ? "" : " " + offClass,
                    raysize ? " switch-" + raysize : "",
                    disabled ? " " + disabledClass : "",
                    '"',
                    '>',
                    '<div class="switch-anim-box">',
                    '<span class="switch-title switch-0">' + title[0] + '</span>',
                    '<span class="switch-hit">&nbsp;</span>',
                    '<span class="switch-title switch-1">' + title[1] + '</span>',
                    '</div>',
                    '</div>'
                ].join('')).insertAfter($elem),
                    maxWidth = Math.max($main.find(".switch-0").outerWidth(), $main.find(".switch-1").outerWidth()),
                    $anim = $main.find(".switch-anim-box").css("left", isOn ? "0" : -1 * maxWidth + "px");

                $elem.val(value[isOn ? 0 : 1]);
                $main.outerWidth(maxWidth + $main.find(".switch-hit").outerWidth() + borderW)
                    .data("value", value).data("title", title)
                    .find(".switch-title").outerWidth(maxWidth);

                //绑定事件
                $main.on("click." + plugName, function () {
                    if ($(this).hasClass(disabledClass)) return;
                    var isOff = $main.hasClass(offClass);
                    $elem.val(value[isOff ? 0 : 1]);
                    $main[(isOff ? "remove" : "add") + "Class"](offClass);
                    $anim.animate({ "left": isOff ? "0" : -1 * maxWidth + "px" }, 200);
                });
            });
        };

    var switchBtn = {
        on: function (event, callback, container) {
            if (event !== "switch" || typeof callback !== "function") return this;
            container = container || "body";
            $(container).on("click." + plugName, selector, function (e) {
                var $this = $(this),
                    $elem = $this.prev(),
                    value = $this.data("value"),
                    title = $this.data("title"),
                    rayevent = $elem.attr("ray-event"),
                    curr = $this.hasClass(offClass) ? 1 : 0;

                //如果返回false则不执行按钮操作
                rayevent && callback.call(this, {
                    event: rayevent,
                    elem: $elem,
                    value: value[curr],
                    title: title[curr],
                    e: e
                }) === false && switchBtn.toggle($elem);
            });
            return this;
        },
        off: function (event, container) {
            if (event !== "switch") return this;
            container = container || "body";
            $(container).off("click." + plugName, selector);
            return this;
        },
        enable: function (elems) {
            $(elems).each(function () {
                $(this).prop("disabled", false).next().removeClass(disabledClass);
            });
            return this;
        },
        disable: function (elems) {
            $(elems).each(function () {
                $(this).prop("disabled", true).next().addClass(disabledClass);
            });
            return this;
        },
        setReadonly: function(elems, readflag) {
            $(elems).each(function () {
                $(this).prop("readonly", readflag).next()[readflag ? "addClass" : "removeClass"](disabledClass);
            });
            return this;
        },
        switchOn: function (elem) {
            return this.toggle(elem, false);
        },
        switchOff: function (elem) {
            return this.toggle(elem, true);
        },
        toggle: function (elem, toOff) {
            $(elem).not("[disabled]").not("[readonly]").each(function () {
                var $this = $(this),
                    $main = $(this).next(),
                    value = $main.data("value"),
                    $anim = $main.find(".switch-anim-box"),
                    ww = $main.find(".switch-0").outerWidth(),
                    curOff = $main.hasClass(offClass),
                    tmpToOff = toOff === undef ? !curOff : toOff;
                if (curOff === tmpToOff) return;
                if (curOff) {
                    $this.val(value[0]);
                    $main.removeClass(offClass);
                    $anim.animate({ "left": "0" }, 200);
                } else {
                    $this.val(value[1]);
                    $main.addClass(offClass);
                    $anim.animate({ "left": -1 * ww + "px" }, 200);
                }
            });
            return this;
        },
        click: function (elem) {
            $(elem).each(function () {
                $(this).next().click();
            });
            return this;
        },
        render: function (container) {
            render(container);
        }
    };

    exports(plugName, switchBtn);
}, rayui.jsAsync());
