/*
 @Name: radiobtn插件
 @Author: ray
 @License: MIT
*/
; rayui.define(function (exports) {
    "use strict";

    var $ = rayui.$, $body,
        plugName = "radiobtn",
        checkedClass = "radiobtn-checked",
        disabledClass = "rayui-disabled",
        selector = '.rayui-radiobtn-box:not(".' + disabledClass + '")',
        render = function (container) {
            //初始化checkbox
            var $objs = container ? $(".rayui-radiobtn", container) : $(".rayui-radiobtn");
            $objs.each(function () {
                var that = this, $elem = $(that);
                if ($elem.data("render")) $elem.next().remove();
                $elem.data("render", true);
                var name = $elem.prop("name"),
                    disabled = $elem.attr("disabled") || $elem.attr("readonly"),
                    skin = $elem.attr("skin"),
                    checked = that.checked,
                    title = $elem.attr("ray-title") || "";

                //创建dom
                var $main = $(['<div class="rayui-radiobtn-box',
                    checked ? " " + checkedClass : "",
                    disabled ? " " + disabledClass : "",
                    '"',//end class
                    ' skin=' + (skin || "default") + '>',
                    '<i class="ra ra-circle radiobtn-icon"></i>',
                    '</div>'
                ].join('')).insertAfter($elem);
                if (title !== "") {
                    $('<span class="radiobtn-title">' + title + '</span>').appendTo($main);
                }
                //绑定事件
                $main.on("click." + plugName, function () {
                    if ($(this).hasClass(disabledClass)) return;
                    //排除其他
                    name && $body.find('.rayui-radiobtn[name="' + name + '"]').each(function () {
                        $(this).next().removeClass(checkedClass);
                    });
                    $(this).addClass(checkedClass);
                    that.checked = true;
                });

            });
        };

    var radiobtn = {
        on: function (event, callback, container) {
            if (event !== "check" || typeof callback !== "function") return this;
            container = container || "body";
            $(container).on("click." + plugName, selector, function (e) {
                var $this = $(this),
                    $elem = $this.prev(),
                    name = $elem.prop("name"),
                    value = $elem.prop("value");

                callback.call(this, {
                    elem: $elem,
                    name: name,
                    value: value,
                    e: e
                });
            });
            return this;
        },
        off: function (event, container) {
            if (event !== "check") return this;
            container = container || "body";
            $(container).off("click." + plugName, selector);
            return this;
        },
        enable: function (elems) {
            $(elems).each(function () {
                $(this).prop("disabled", false).next().removeClass(disabledClass);
            });
            return this;
        },
        disable: function (elems) {
            $(elems).each(function () {
                $(this).prop("disabled", true).next().addClass(disabledClass);
            });
            return this;
        },
        setReadonly: function (elems, readflag) {
            $(elems).each(function () {
                $(this).prop("readonly", readflag).next()[readflag ? "addClass" : "removeClass"](disabledClass);
            });
            return this;
        },
        check: function (elem) {
            $(elem).next().click();
            return this;
        },
        render: function (container) {
            render(container);
        }
    };

    $(function () {
        $body = $("body");
    });

    exports(plugName, radiobtn);
}, rayui.jsAsync());
