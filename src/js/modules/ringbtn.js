/*
 @Name: ringbtn插件
 @Author: ray
 @License: MIT
*/
; rayui.define(function (exports) {
    "use strict";

    var $ = rayui.$,
        plugName = "ringbtn",
        globalIndex = 1000,
        dataCache = {},
        globalOptions = {
            outerRadius: 50,//外半径
            ringColor:"#0b9869",//外圆颜色
            innerRadius: 40,//内半径
            innerColor: "#fff",//内圆颜色
            fontSize: 20,//字体大小
            fontColor: "#000",//字体颜色
        },
        DealClass = function (options) {
            var that = this;
            options.index = globalIndex;
            that.options = $.extend(true, {}, globalOptions, options);
            DealClass.utils.init.call(that);
            DealClass.utils.event.call(that);
        };

    DealClass.utils = {
        init: function () {
            var that = this,
                options = that.options;

            var outerd = options.outerRadius * 2;
            options.$main = $(options.elem).addClass('rayui-ringbtn-box').css({
                width: outerd,
                height: outerd
            });
            //外圆
            options.$outer = $('<div class="ringbtn-outerbox"/>').css({
                padding: options.outerRadius - options.innerRadius + 'px'
            }).appendTo(options.$main);
            DealClass.utils.setRingColor.call(that);

            //内圆
            options.$inner = $('<div class="ringbtn-innerbox"/>').css({
                "background-color": options.innerColor
            }).appendTo(options.$outer);
            //添加label
            if (options.label) {
                //找出圆内最大正方形，正方形边长a=√2r，padding=r-a/2
                var padding = options.innerRadius - 0.707 * options.innerRadius;
                options.$inner.css("padding", padding + "px");

                var innerHeight = options.$inner.height();
                options.$label = $('<div class="ringbtn-label">' + options.label + '</div>').css({
                    width: innerHeight,
                    height: innerHeight,
                    "font-size": options.fontSize + "px",
                    "color": options.fontColor
                }).appendTo(options.$inner);
            }

        },
        setRingColor: function () {
            var that = this,
                options = that.options,
                direction = options.direction;

            var cssClass = 'ringbtn-index-' + options.index;
            !options.$style && (options.$style = $('<div/>').appendTo(options.$outer.addClass(cssClass)));
            //外圆样式
            var outerCss = [];
            outerCss.push('<style>');
            outerCss.push('.' + cssClass + '{');
            if (direction) {
                var color = options.ringColor.indexOf(',') > 0 ? options.ringColor : options.ringColor + "," + options.ringColor;
                outerCss.push('background:-webkit-linear-gradient(' + direction + ', ' + color + ');');
                outerCss.push('background:-o-linear-gradient(' + direction + ', ' + color + ');');
                outerCss.push('background:-moz-linear-gradient(' + direction + ', ' + color + ');');
                outerCss.push('background:linear-gradient(' + direction + ', ' + color + ');');
            } else {
                outerCss.push('background-color:' + options.ringColor);
            }
            outerCss.push('}');
            outerCss.push('</style>');
            options.$style.html(outerCss.join(''));
        },
        event: function () {
            var that = this,
                options = that.options;

            if (options.$inner && typeof options.click === "function")
                options.$inner.on("click." + plugName, options.click);

        }
    }

    DealClass.prototype = {

        setRingColor: function (color, direction) {
            var options = this.options;
            options.ringColor = color;
            direction && (options.direction = direction);
            DealClass.utils.setRingColor.call(this);
            return this;
        },
        setInnerColor: function (color) {
            var options = this.options;
            options.innerColor = color;
            options.$inner.css("background-color", color);
            return this;
        },
        setLabel: function (text, color) {
            var options = this.options;
            options.label = text;
            options.$label.html(text);
            if (color) {
                options.FontColor = color;
                options.$label.css("color", color);
            }
            return this;
        }
    }

    var ringbtn = {
        options: globalOptions,
        render: function (options) {
            var obj = options.elem;
            if ($(obj).length === 0) return "DOM对象不存在";

            var cc = new DealClass(options);
            dataCache[globalIndex++] = cc;
            return cc;
        }
    };

    exports(plugName, ringbtn);
}, rayui.jsAsync());
