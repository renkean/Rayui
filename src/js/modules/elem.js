/*
 @Name: elem插件
 @Author: ray
 @License: MIT
 */
; rayui.define("layer", function (exports, undef) {
    "use strict";

    var $ = rayui.$,
        layer = rayui.layer,
        $win = $(window),
        plugName = "elem",
        globalIndex = 1000,
        fullHeightCache = {},
        resizeIndex = 0;
        

    /**
     * 高度最大化
     * @param {any} heightOpt 数值|full-数值|top-数值|sel-selector-数值
     */
    $.fn.fullHeight = function (heightOpt) {
        var options = {
            $elem: $(this),
            heightSetting: heightOpt || "top-0"
        }, fullHeightGap;

        var func = function () {
            if (/^full-\d+$/.test(options.heightSetting)) {//full-差距值
                fullHeightGap = options.heightSetting.split('-')[1];
                options.height = $win.height() - fullHeightGap;
            } else if (/^top-\d+$/.test(options.heightSetting)) {//top-差距值
                fullHeightGap = options.$elem.offset().top;
                fullHeightGap += parseInt(options.heightSetting.split('-')[1]);
                options.height = $win.height() - fullHeightGap;
            } else if (/^sel-[#|\.][\w]+-\d+$/.test(options.heightSetting)) { //sel-id序列-差距值
                fullHeightGap = options.$elem.offset().top;
                var list = options.heightSetting.split('-');
                $("" + list[1]).each(function () {
                    fullHeightGap += $(this).outerHeight();
                });
                if (list.length === 3) fullHeightGap += parseInt(list[2]);
                options.height = $win.height() - fullHeightGap;
            }
            options.$elem.outerHeight(options.height);//数据设置高度
        }
        func();
        fullHeightCache[globalIndex++] = func;
    }

    /**
     * 全屏显示
     */
    $.fullScreen = function () {
        var docElm = document.documentElement;
        //W3C  
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        }
        //FireFox  
        else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        }
        //Chrome等  
        else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        }
        //IE11
        else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
        layer.msg('按Esc即可退出全屏', { timeout: 2000 });
    }

    var handle = {
        setSameHeight: function () {
            $(".sameHeight").each(function () {
                var maxheight = 0,
                    $cols = $(this).find(">[class^=col-]");
                $cols.css("height", "");
                $cols.each(function () {
                    var h = $(this).height();
                    h > maxheight && (maxheight = h);
                });
                $cols.height(maxheight);
            });
        }
    }

    $(function () {
        $win.resize(function () {
            if ($(".sameHeight").length > 0) {
                clearTimeout(resizeIndex);
                resizeIndex = setTimeout(handle.setSameHeight, 10);
            }
            //设置自动高度
            for (var key in fullHeightCache)
                fullHeightCache[key]();
        });
        handle.setSameHeight();
    });

    exports(plugName, handle);
}, rayui.jsAsync());
