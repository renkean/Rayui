/*
 @Name: form公共插件
 @Author: ray
 @License: MIT
*/
; (function () {
    "use strict";

    /**
     * 回车移动到下一个文本框
     * @param {any} submitbtn 提交按钮
     */
    $.fn.registerEnterKey = function (submitbtn) {
        var objs = $(this).find("input[type=text],input[type=password],textarea");
        if (objs.length === 0) return;
        function getNextVisibleObj(index) {
            if (index++ === objs.length - 1) return null;
            var obj = $(objs).eq(index);
            if (obj.is(":visible")) return obj;
            return getNextVisibleObj(index);
        }

        objs.each(function (a, b) {
            $(b).on("keypress.rayui", function (e) {
                if (e.keyCode === 13) {
                    var nextObj = getNextVisibleObj(a);
                    if (nextObj == null) {
                        $(this).blur();
                        $(submitbtn).click();
                    } else {
                        nextObj.focus();
                        e.preventDefault();//屏蔽回车提交表单
                    }
                }
            });
        });

    }

    /**
     * 参数序列化为json格式
     */
    $.fn.serializeJson = function () {
        var serializeObj = {};
        var array = this.serializeArray(),
            getVal = function (val) {
                return val === "" ? val : isNaN(val) ? val : val * 1;
            };
        $(array).each(function () {
            if (serializeObj[this.name]) {
                if ($.isArray(serializeObj[this.name])) {
                    serializeObj[this.name].push(getVal(this.value));
                } else {
                    serializeObj[this.name] = [serializeObj[this.name], getVal(this.value)];
                }
            } else {
                serializeObj[this.name] = getVal(this.value);
            }
        });
        return serializeObj;
    };

    /**
     * 获取选择器值或属性值数组
     * @param {any} attr 属性名称，不填时取text值
     */
    $.fn.getArray = function (attr) {
        var list = [];
        $.each($(this), function (a, b) {
            if (attr) {
                b.hasAttribute(attr) && list.push($(b).attr(attr));
            } else {
                list.push($(b).is("input,select,textarea") ? $(b).val() : $(b).text());
            }
        });
        return list;
    }

    /**
     * 拼接选择器数值
     * @param {any} c 拼接字符
     * @param {any} attr 属性名称，不填时取text值
     */
    $.fn.join = function (c, attr) {
        var str = $(this).getArray(attr);
        return str.join(c);
    }
    
    /**
     * 阻止自动填充
     * @param {any} time 移除时间，单位毫秒，默认500毫秒
     * @param {any} callback 回调函数
     * @param {any} isStrength 是否开启增强模式
     */
    $.fn.preventAutoInput = function (time, callback, isStrength) {
        if (typeof time === "function") {
            isStrength = callback;
            callback = time;
            time = 500;
        } else if (typeof time === "boolean") {
            isStrength = time;
            time = 500;
        } else if (typeof time !== "number") {
            time = 500;
        }
        var $form = $(this),
            addElem = function ($con) {
                if (!$con.data("isAdded")) {
                    $con.data("isAdded", true);
                    var time = new Date().getTime();
                    $con.prepend('<div class="raypreventAutoInputDivStart" style="position: absolute;left:-10000px;"><input type="text" name="username' + time +'" /><input type="password" name="password' + time+'" /></div>');
                    $con.append('<div class="raypreventAutoInputDivEnd" style="position: absolute;left:-10000px;"><input type="password" name="password' + time +'" /></div>');
                }
            }, removeElem = function ($con) {
                if ($con.data("isAdded")) {
                    $con.find(">.raypreventAutoInputDivStart").remove();
                    $con.find(">.raypreventAutoInputDivEnd").remove();
                    $con.data("isAdded", false);
                }
            };

        addElem($form);
        setTimeout(function () {
            removeElem($form);
            typeof callback === "function" && callback();
            //如果是加强模式，屏蔽首次填充后，再添加一次元素
            if (isStrength) addElem($form);
        }, time);

        //屏蔽360浏览器点击密码自动填充，360比较特殊点击密码框就会自动填充
        if (isStrength) {
            if ($form.is("form")) {
                $form.submit(function () {
                    removeElem($(this));
                });
            }
        }

        return $form;
    }

    window.rayui && rayui.define(function (exports) {
        exports("form", {});
    }, rayui.jsAsync());


})();

