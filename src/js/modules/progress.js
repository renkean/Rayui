/*
 @Name: progress插件
 @Author: ray
 @License: MIT
 */
; rayui.define(function (exports, undef) {
    "use strict";

    var $ = rayui.$,
        plugName = "progress",
        globalIndex = 1000,
        dataCache = {},
        bgcolorcss = 'background-color',
        progressOption = {
            animSpeed: 300, //动画执行时间，对应css里的transition动画时间
            max: 100,
            min: 0
        };

    /**
     * 进度条
     * @param {any} options
     */
    $.fn.progress = function (options) {
        var $this = $(this).addClass("rayui-progress"), index = $this.data("index");
        if (index > 0) return dataCache[index];

        if (typeof options === "number") {
            options = { progress: options };
        } else {
            options = options || {};
        }

        options.height = options.height || 10;
        options.bgcolor && $this.css("background", options.bgcolor);
        options = $.extend(true, {}, progressOption, options);
        options.index = index = globalIndex++;
        var leftcss = "rayui-progress-text-left",
            barCss = {
                width: "0%",
                height: options.height + "px",
                "line-height": options.height + "px"
            }, hasProcessColor = $.isArray(options.processColors), pColorIndex = [], pColors = [],
            setProcessColors = function () {
                if (!hasProcessColor) return;
                var len = pColorIndex.length, i = 0, index;
                while (i < len)
                    if (options.progress > pColorIndex[i++]) index = i - 1;
                options.$bar.css(bgcolorcss, index === undef ? options.color : pColors[index]);
            }, len, i, p;

        if (hasProcessColor) {
            len = options.processColors.length;
            var o;
            for (i = 0; i < len; i++) {
                o = options.processColors[i];
                for (var key in o) {
                    p = parseFloat(key);
                    if (i === 0 && p !== options.min) {
                        pColorIndex.push(options.min);
                        pColors.push('#d43838');//红色
                    }
                    pColorIndex.push(p);
                    pColors.push(o[key]);
                    if (i === len - 1 && p !== options.max) {
                        pColorIndex.push(options.max);
                        pColors.push('#2e8b57');//绿色
                    }
                }
            }
        }

        //渲染
        options.color && (barCss[bgcolorcss] = options.color);
        options.$bar = $('<div class="rayui-progress-bar"/>').css(barCss).appendTo($this);

        if (options.label) {
            //top middle bottom
            options.$label = $('<span class="rayui-progress-text rayui-pglabel-' + options.label + '"></span>').appendTo(options.$bar);
            var minH = Math.max(options.$label.height(), 16);
            $this.css("margin-" + options.label, minH + "px");
            options.label === "top" && options.$label.css("bottom", (minH + options.height) / 2 + "px");
            options.label === "bottom" && options.$label.css("top", (minH + options.height) / 2 + "px");
        }
        var obj = dataCache[index] = {
            setLabel: function (str, color) {
                if (!options.label) return this;
                options.$label.removeClass(leftcss).text(str);
                //判断宽度
                setTimeout(function () {
                    options.$label.width() > options.$bar.width() && options.$label.addClass(leftcss);
                }, options.animSpeed);
                color && options.$label.css("color", color);
                return this;
            },
            setProgress: function (cur, lblflag) {
                if (cur < options.min || cur > options.max) cur = options.min;
                options.progress = cur;
                var percent = (cur - options.min) * 100 / (options.max - options.min), p = "" + percent, index = p.indexOf('.');
                if (index >= 0 && p.substr(index).length > 2) percent = percent.toFixed(2);
                percent += "%";
                //设置过程颜色
                setProcessColors();
                //动画
                //options.$bar.css("width", percent);//这样写不知道为什么首次无动画
                setTimeout(function () { options.$bar.css("width", percent) }, 0);
                (lblflag === undef ? options.label : lblflag) && this.setLabel(percent);
                return this;
            },
            setColor: function (color) {
                options.$bar.css(bgcolorcss, color);
                return this;
            }
        };

        obj.setProgress(options.progress, true);
        return obj;
    }

    var handle = {
        progressOption: progressOption,
        render: function (options) {
            if ($(options.elem).length === 0) return "DOM对象不存在";
            return $(options.elem).progress(options);
        }
    }

    exports(plugName, handle);
}, rayui.jsAsync());
