# Rayui

#### 项目介绍
参照各大网站和部分经典框架，为了提升自己的能力，写了一套前端js框架

#### 已经上传源码
修复了多处bug，现已上传源码。如遇bug欢迎发送至邮箱 lll46285@126.com

#### 一睹为快

体验地址：[www.rayui.cn](http://www.rayui.cn)

#### form
![输入图片说明](https://images.gitee.com/uploads/images/2018/1010/153138_287d48a0_1428435.png "form1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1008/172958_b7b04f6b_1428435.png "form2.png")

#### calender
![输入图片说明](https://images.gitee.com/uploads/images/2018/0930/173627_9fbfd28a_1428435.png "calender.png")

#### navbar
![输入图片说明](https://images.gitee.com/uploads/images/2018/0930/173643_1f613531_1428435.png "navbar.png")

#### select
![输入图片说明](https://images.gitee.com/uploads/images/2018/0930/173658_acb8a29f_1428435.png "select.png")

#### selecttree
![输入图片说明](https://images.gitee.com/uploads/images/2018/0930/173719_e296c849_1428435.png "selecttree.png")

#### table
![输入图片说明](https://images.gitee.com/uploads/images/2018/0930/173743_9a8fd2f0_1428435.png "table.png")

#### treegrid
![输入图片说明](https://images.gitee.com/uploads/images/2018/0930/173754_85081744_1428435.png "treegrid.png")

#### tree
![输入图片说明](https://images.gitee.com/uploads/images/2018/0930/173809_ba683e6b_1428435.png "tree.png")


#### 参考框架与网站
<p>1、<a target="_blank" href="https://www.layui.com">layui--经典模块化前端框架<img src="images/hot.gif"/></a></p>
<p>2、<a target="_blank" href="http://www.bootcss.com">Bootstrap--简洁、直观、强悍的前端开发框架</a></p>
<p>3、<a target="_blank" href="http://fontawesome.dashgame.com">Font Awesome--一套绝佳的图标字体库和CSS框架</a></p>
<p>4、<a target="_blank" href="http://www.jeasyui.com">EasyUI--EasyUI framework helps you build your web pages easily</a></p>
<p>5、<a target="_blank" href="http://www.treejs.cn/v3/main.php">zTree--zTree 是一个依靠 jQuery 实现的多功能 “树插件”</a></p>

